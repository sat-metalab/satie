TestChannelLayout : SatieUnitTest {
	var server;

	setUp {
		server = Server(this.class.name);
	}

	tearDown {
		server.remove;
	}

	test_outputChannels {
		var satie;
		var timeout, cond = Condition.new;
		var answer, results, buffer, recorder;
		var layout, spat, config;
		var numSamples = 64;
		var numChans = 4;
		var indexes = Array.iota(numChans);

		answer = indexes.odd; // array of true/false
		layout = indexes.select { |i| i.odd }; // only odd channels will be used
		spat = Array.fill(layout.size, \monoSpat); // one monoSpat per layout channel
		config = SatieConfiguration(
			server: server,
			listeningFormat: spat,
			outBusIndex: layout,
			minOutputBusChannels: numChans
		);
		satie = Satie(config);
		this.boot(satie);

		// create a source
		satie.makeInstance(\foo, \testtone, synthArgs: [\gainDB, -9]);

		// create the recorder synth and buffer
		buffer = Buffer.alloc(server, numSamples, numChans);
		server.sync;

		SynthDef(\rec, {
			var env = EnvGen.ar(Env([0, 1], [numSamples * SampleDur.ir]), doneAction: Done.freeSelf);
			var input = In.ar(0, numChans) * env; // from the first output channel index to numChans-1
			BufWr.ar(
				MovingAverage.rms(input),
				buffer,
				env * numSamples, // how many samples will be written
				loop: 0
			);
			// no need to have an Out
		}).add;
		server.sync;

		// create a Synth that won't play immediately
		recorder = Synth.newPaused(\rec, target: server, addAction: 'addToTail');
		// when recorder synth sends /n_end message, unhang the condition
		OSCFunc({ cond.unhang }, '/n_end', server.addr, argTemplate: [recorder.nodeID]).oneShot;
		server.sync;

		timeout = fork { 3.wait; cond.unhang };
		// trigger the recoder and wait for unhang
		recorder.run;
		cond.hang;
		timeout.stop;

		// get numChans values starting from the middle index of the buffer
		buffer.getToFloatArray((numSamples * 0.5).asInteger, numChans, action: { |array| results = array });
		server.sync;

		this.assertEquals(
			results > 0.0,
			answer,
			"Satie is playing on all configured output channels"
		);

		this.quit(satie);
	}
}
