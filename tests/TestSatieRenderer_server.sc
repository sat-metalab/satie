TestSatieRenderer_server : SatieUnitTest {

	var server, satie;

	setUp {
		var config;

		server = Server(this.class.name);
		// avoid "exceeded number of interconnect buffers" errors when compiling plugins
		server.options.numWireBufs = 1024;

		config = SatieConfiguration(
			server: server,
			listeningFormat: [\stereoListener],
			ambiOrders: [1, 3]
		);
		satie = Satie(config);

		this.boot(satie);
	}

	tearDown {
		this.quit(satie);
		server.remove;
	}

	test_makeAmbiFromMono_source_plugin {
		satie.makeAmbiFromMono(\foo, \testtone, hoaEncoderType: \harmonic, ambiOrder: 3, ambiBus: satie.config.ambiBus[1]);
		this.assertEquals(satie.generators[\foo], \testtone, "Satie's generators dictionary should include keyValue pair (\\foo -> \\testtone)");
	}

	test_makeAmbiFromMono_effects_plugin {
		satie.makeAmbiFromMono(\foo, \Limiter, hoaEncoderType: \harmonic, ambiOrder: 3, ambiBus: satie.config.ambiBus[1]);
		this.assertEquals(satie.effects[\foo], \Limiter, "Satie's effects dictionary should include keyValue pair (\\foo -> \\Limiter)");
	}

	test_makeAmbiFromMono_nil_hoaEncoderType {
		var ugens;

		// leaving hoaEncoderType nil should cause config.hoaEncoderType to be chosen
		satie.makeAmbiFromMono(\foo, \testtone, hoaEncoderType: nil, ambiOrder: 3, ambiBus: satie.config.ambiBus[1]);
		ugens = satie.synthDescLib.synthDescs[\foo].def.children.collect { |ugen| ugen.class.name.asSymbol };
		this.assert(ugens.includes(\HOAEncoder3), "Leaving hoaEncoderType Nil should cause Satie.config.hoaEncoderType to be used")
	}

	test_makeAmbiFromMono_set_hoaEncoderType {
		var ugens;

		satie.makeAmbiFromMono(\foo, \testtone, hoaEncoderType: \harmonic, ambiOrder: 3, ambiBus: satie.config.ambiBus[1]);
		ugens = satie.synthDescLib.synthDescs[\foo].def.children.collect { |ugen| ugen.class.name.asSymbol };

		// we can't test for the inclusion of HOASphericalHarmonics
		// When compiled into a SynthDef, this pseudo-UGen gets disolved into a bunch of BinaryOpUGens
		// all we can do is test that HOAEncoder is not used inside this SynthDef
		this.assertEquals(ugens.includes(\HOAEncoder3), false, "The specified hoaEncoderType should be used when generating SynthDefs");

		satie.makeAmbiFromMono(\bar, \testtone, hoaEncoderType: \lebedev50, ambiOrder: 3, ambiBus: satie.config.ambiBus[1]);
		ugens = satie.synthDescLib.synthDescs[\bar].def.children.collect { |ugen| ugen.class.name.asSymbol };

		this.assertEquals(ugens.includes(\HOAEncLebedev503), true, "The specified hoaEncoderType should be used when generating SynthDefs")
	}

	test_findPlugin_error {
		this.assertException({ satie.findPlugin(\fakeplugin) }, Error, "An Error message")
	}

	test_validateAmbiOrder_error {
		this.assertException({ satie.validateAmbiOrder(3.0) }, Error, "Non Integer ambisonic orders should throw an Exception");
		this.assertException({ satie.validateAmbiOrder(2) }, Error, "Only configured ambisonic orders should be allowed");
	}

	test_validateAmbiOrder {
		this.assertNoException({ satie.validateAmbiOrder(3) }, "Choosing a configured ambisonic order should not throw Exception")
	}

	test_validateAmbiBus {
		this.assertNoException(
			{ satie.validateAmbiBus(3, Bus.audio(server, 16)) },
			"Providing a Bus with a sufficient number of channels for the specidifed order should not throw an Exception"
		);
	}

	test_validateAmbiBus_error {
		this.assertException(
			{ satie.validateAmbiBus(3, Bus.audio(server, 9)) },
			Error,
			"Providing a Bus with an insufficient number of channels should throw an Exception"
		);
	}

	test_findPlugin {
		var source = satie.findPlugin(\testtone);
		var effect = satie.findPlugin(\Limiter);

		this.assertEquals(source[0].class, SatiePlugin, "The first returned value should be a SatiePlugin");
		this.assertEquals(source[1], \source, "The second returned value should be the symbol 'source'");
		this.assertEquals(effect[0].class, SatiePlugin, "The first returned value should be a SatiePlugin");
		this.assertEquals(effect[1], \effect, "The second returned value should be the symbol 'effect'");
	}

	test_addPluginToDictionary {
		satie.addPluginToDictionary(\source, \testname, \testplugin);
		satie.addPluginToDictionary(\effect, \testname, \testplugin);
		this.assertEquals(satie.generators[\testname], \testplugin, "Source plugin should have been added to the generators dictionary");
		this.assertEquals(satie.effects[\testname], \testplugin, "Effect plugin should have been added to the effects dictionary");
	}

	test_makeSynthDef_source_plugin {
		satie.makeSynthDef(\foo, \testtone, spatSymbolArray: satie.config.listeningFormat);
		this.assertEquals(satie.generators[\foo], \testtone, "Satie's generators dictionary should include keyValue pair (\\foo -> \\testtone)");
	}

	test_makeSynthDef_effects_plugin {
		satie.makeSynthDef(\foo, \Limiter, spatSymbolArray: satie.config.listeningFormat);
		this.assertEquals(satie.effects[\foo], \Limiter, "Satie's effects dictionary should include keyValue pair (\\foo -> \\Limiter)");
	}

	test_makeInstance_default_arguments {
		var synth, instances, getFreq;

		// by default, the synth is created in the \default group
		synth = satie.makeInstance(\foo, \testtone);
		this.assert(synth.class == Synth, "Instance of Synth(\foo) was created");

		instances = satie.groupInstances[\default];
		this.assert(instances.includesKey(\foo), "Synth(\foo) should have been added to the instance dictionary");
		this.assertEquals(instances.at(\foo), synth, "Instance dictionary key \\foo should point to synth \\foo");

		// \testtone's single argument is called \sfreq and has the default value 200
		// get is asynchronous, so we sync
		synth.get(\sfreq, { |val| getFreq = val });
		server.sync;
		this.assertEquals(getFreq, 200.0, "The plugin's default argument values should be preserved");
	}

	test_makeSatieGroup_default_arguments {
		var newGroup, groups;

		// by default, new Satie groups are added to the head of the server's default group
		// normally, this means above Satie's \default group
		newGroup = satie.makeSatieGroup(\foo);
		this.assertEquals(newGroup.class, ParGroup, "makeSatieGroup should return a ParGroup");
		groups = satie.groups;
		this.assert(groups.includesKey(\foo), "Satie's groups should include a group called 'foo'");
		this.assertEquals(groups[\foo], newGroup, "Groups dictionary key \\foo should point to group \\foo");
	}

	test_killSatieGroup {
		var group;

		satie.makeSatieGroup(\foo);
		this.assert(satie.groups.includesKey(\foo), "There should be a group called 'foo'");
		satie.killSatieGroup(\foo);
		this.assertEquals(satie.groups.includesKey(\foo), false, "Killed groups should no longer be found in groups dictionary");
		this.assertEquals(satie.groupInstances[\foo], nil, "Killed groups should no longer be found in groupInstances");
	}

	test_cleanInstance {
		satie.makeInstance(\foo, \testtone);
		this.assert(satie.groupInstances[\default].keys.includes(\foo), "A synth should be in the default group");
		satie.cleanInstance(\foo);
		server.sync;
		this.assertEquals(satie.groupInstances[\default][\foo], nil, "Synth instance should no longer be found in the \\default group");
		this.assertEquals(satie.namesIds[\foo], nil, "Synth nodeID should no longer be found within Satie's namesIds dictionary");
	}

	test_processBufnum {
		var synthArgs, result;

		satie.loadSample(\foo, Platform.resourceDir +/+ "sounds/a11wlk01.wav");
		server.sync;

		// test without bufum argument
		synthArgs = [\freq, 440, \amp, 0.1];
		result = satie.processBufnum(synthArgs);
		this.assertEquals(
			result,
			synthArgs,
			"processBufnum should return synthArgs unaltered when no bufnum found"
		);

		// test bufnum is not a Symbol
		synthArgs = synthArgs ++  [\bufnum, 100];
		result = satie.processBufnum(synthArgs);
		this.assertEquals(
			result,
			synthArgs,
			"processBufnum should return synthArgs unaltered when bufnum isn't a Symbol"
		);

		// test bufnum is a Symbol that exists
		synthArgs = synthArgs[..4] ++ \foo;
		result = satie.processBufnum(synthArgs);
		this.assertEquals(
			result,
			synthArgs[..4] ++ satie.audioSamples[\foo].bufnum,
			"processBufnum should return synthArgs with bufnum value replaced with actual bufnum"
		);

		// test bufnum is a unknown Symbol
		synthArgs = synthArgs[..4] ++ \foobar;
		this.assertException(
			{ satie.processBufnum(synthArgs) },
			Error,
			"processBufnum should throw an exception if bufnum is not found in audioSamples dictionary"
		);
	}

}

