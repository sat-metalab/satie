TestSatiePlugins : SatieUnitTest {

	var server, satie;

	setUp {
		server = Server(this.class.name);
		satie = Satie(SatieConfiguration(server));
		this.boot(satie);
	}

	tearDown {
		this.quit(satie);
		server.remove;
	}

	test_sources_mono_output {
		var sources = satie.config.sources.select { |plugin| plugin.channelLayout == \mono };
		sources.do { |plugin|
			var name = plugin.name;
			var data = plugin.function.asSynthDef.asSynthDesc.outputData;
			this.assertEquals(data.size, 1, "SatiePlugin: % has exactly one output".format(name));
			this.assertEquals(data[0][\numChannels], 1, "SatiePlugin: % has exactly one output channel".format(name));
		}
	}

}

