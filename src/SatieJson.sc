// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.


SatieJson {
	classvar <newline, <tab;

	*initClass {
		newline = [$\\, $\\, $n].as(String);
		tab = [$\\, $\\, $t].as(String);
	}

	*stringify { | obj |

		case
		{ obj.isString } { ^obj.asCompileString.replace("\n", newline).replace("\t", tab) }
		{ obj.class === Symbol} { ^this.stringify(obj.asString) }
		{ obj === true } { ^"true" }
		{ obj === false } { ^"false" }
		{ obj.isNil } { ^"null" }
		{ obj === inf } { ^"null" }
		{ obj === -inf } { ^"null" }
		{ obj.isNumber } {
			case
			{ obj.isNaN } { ^"null" }
			{ ^obj.asString }
		}
		{ obj.isKindOf(SequenceableCollection) } { ^"[ % ]".format(this.fromArray(obj)) }
		{ obj.isKindOf(Dictionary) } { ^this.fromDict(obj) }
		{ obj.isKindOf(Set) } { ^"[ % ]".format(this.fromArray(obj.as(Array))) }
	}

	*fromArray { | obj |
		^obj.collect({ | item |
			this.stringify(item)
		}).join(", ");
	}

	*fromDict { | obj |
		var ret = List.new;
		obj.keysValuesDo({ | key, val |
			ret.add( "%: %".format(key.asString.quote,  this.stringify(val)));
		});
		^"{ % }".format(ret.join(", "));
	}

	*parseJsonString { |string|
		var json;

		try {
			json = this.parseJsonConfig(string.parseJSON);
		} { |e|
			e.errorString.error;
			"%: Unable to parse JSON. Please check that the syntax is correct".format(thisMethod).error;
		};

		^json;
	}

	*parseJsonFile { |path|
		var json;

		try {
			json = this.parseJsonConfig(path.parseJSONFile);
		} { |e|
			e.errorString.error;
			"%: Unable to parse JSON. Please check that the syntax is correct".format(thisMethod).error;
		};

		^json;
	}

	*parseJsonConfig { |json|
		var dict;

		if(json.class !== Dictionary) {
			Error("%: parsed JSON is not a Dictionary".format(thisMethod)).throw;
		};

		dict = json["satie_configuration"];

		if(dict.isNil) {
			Error("%: JSON does not contain an entry named \"satie_configuration\"".format(thisMethod)).throw;
		};

		^this.convertJsonDictionary(dict);
	}

	// recursively converts parseJSON Dictionaries to IdentityDictionaries
	// casts keys into Symbols
	// returns a "known" IdentityDictionary
	*convertJsonDictionary { |dict|
		dict = dict.asAssociations.collectAs(
			class: IdentityDictionary,
			function: { |pair|
				pair.key.asSymbol -> this.handleValueType(pair.value);
			}
		);
		^dict.know_(true);
	}

	*handleValueType { |value|
		^switch(value.class)
			{Nil} { value }
			{Dictionary} { this.convertJsonDictionary(value) }
			{Array} { this.convertJsonArray(value) }
			{ this.convertJsonString(value) };
	}

	*convertJsonArray { |array|
		^array.collect { |value|
			this.handleValueType(value);
		};
	}

	*convertJsonString { |str|
		var value;

		value = switch(str.asSymbol)
			{\true} { true }
			{\false} { false }
			{ nil };

		if(value.notNil) {
			^value;
		};

		value = this.tryNumberConversion(str);

		if(value.notNil) {
			^value;
		} {
			^str.asSymbol;
		};
	}

	*tryNumberConversion { |str|
		var isNumber = false;
		var isFloat = false;
		var chars;

		// if last digit is not 0-9 return early
		if(str.last.isDecDigit.not) {
			^nil;
		};

		// make an array of indicies from 0 to str.size-1
		chars = Array.iota(str.size);

		// drop the index of the point in case of a float
		if(str.includes($.)) {
			isFloat = true;
			chars.removeAt(str.indexOf($.));
		};

		// drop the index of the negative sign
		if(str.first === $-) {
			chars.removeAt(0);
		};

		// check that all remaining chars are 0-9 numbers
		isNumber = chars.every { |index| str[index].isDecDigit };

		if(isNumber.not) {
			^nil;
		} {
			if(isFloat) {
				^str.asFloat;
			} {
				^str.asInteger;
			}
		}
	}

}
