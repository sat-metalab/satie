SatieGUI {
	var context;
	var <>satieGuiWidth;
	var <>satieGuiHeight;
	var <>satieObjectSize;
	var <>satieGuiScale;
	var satieGuiOrigin;
	var origin;
	var vbapSurfaceDiameter;

	var <>satieObjects;

	*new {|satieContext|
		if (satieContext.class == Satie,
			{
				^super.newCopyArgs(satieContext).init;
			},
			{
				"ERROR: Wrong argument, should be Satie".error;
				^"null";
			}
		);
	}

	init {
		satieGuiWidth = 1024;
		satieGuiHeight = 768;
		satieObjectSize = 30;
		satieGuiScale = 1;
		satieGuiOrigin = Point.new(satieGuiWidth/2, satieGuiHeight/2);
		origin = 0@0;
		vbapSurfaceDiameter = satieGuiWidth/2;
		satieObjects = Dictionary.newFrom([
			\windows, Dictionary.new, 
			\spatializers, Dictionary.newFrom([
				\angles, Dictionary.new,
				\color, Dictionary.new,
				\size, Dictionary.new,
				\position, Dictionary.new,
			]), 
			\objects, Dictionary.new,
			\objectsPos, Dictionary.new
		]);
		satieObjects[\spatializers][\angles].clear;
    	satieObjects[\spatializers][\color].clear;

    	context.config.listeningFormat.do( { | item, i |
        	satieObjects[\spatializers][\angles].putPairs(context.inspector.getSpatializerSpeakerAngles(item.asSymbol).getPairs);
        	satieObjects[\spatializers][\color].add(item.asSymbol -> Color.new255(100.rand, 150.rand,255, 0.9*255));
        	satieObjects[\spatializers][\size].add(item.asSymbol -> (satieGuiWidth/2) );
        	satieObjects[\spatializers][\position].add(item.asSymbol -> Dictionary.newFrom([\x, 0, \y, 0, \z, 0] ));
    	});
	}

	createSatieObject { | name, window, pos, rawDiameter, number, colorObject |
		var objectText, posx, posy, diameter;
		posx = satieGuiOrigin.x + pos.x;
		posy = satieGuiOrigin.y + pos.y;
		diameter = rawDiameter * satieGuiScale;
		satieObjects[\objectsPos].add((name++"Front").asSymbol -> Point.new(0,0));
		satieObjects[\objectsPos].add((name++"Top").asSymbol -> Point.new(0,0));
		satieObjects[\objects].add(name -> UserView.new(window,Rect(posx-(diameter/2), posy-(diameter/2),diameter,diameter)));
		satieObjects[\objects][name].drawFunc = {
			Pen.fillColor = colorObject;
			Pen.fillOval(Rect(0,0,diameter,diameter));
		};
		objectText = StaticText(satieObjects[\objects][name], Rect(0,0,diameter,diameter)).string_(number.asString);
		objectText.align = \center;
		objectText.stringColor = Color.white;
		objectText.font = Font("Monaco", 14);
	}

	moveSatieObject { | name, newposx, newposy |
		var posx, posy;
		posx = satieGuiOrigin.x + newposx - (satieObjectSize/2);
		posy = satieGuiOrigin.y - newposy - (satieObjectSize/2);
		satieObjects[\objects][name.asSymbol].moveTo(posx,posy);
	}

	drawOrigin { | window |
		var origin, size;
		size = satieGuiHeight/30;
		origin = UserView.new(window,Rect((satieGuiOrigin.x-(size/2)), (satieGuiOrigin.y-(size/2)),size,size));
		origin.drawFunc = {
			Pen.strokeColor = Color.black;
			Pen.line((size/2)@0, (size/2)@size);
			Pen.line(0@(size/2), size@(size/2));
			Pen.stroke;
		};
	}

	drawLegends { | window |
		satieObjects[\spatializers][\angles].keysValuesDo { |key, value, i|
			var objectText;
			UserView( window,Rect(10, (10+(10*i)+(20*i)), 20, 20)).background_(satieObjects[\spatializers][\color][key.asSymbol]);
			objectText = StaticText(window.view, Rect(35,(10+(10*i)+(20*i)), 200, 20)).string_(key.asString);
			objectText.stringColor = Color.grey;
			objectText.font = Font("Monaco", 14);
		};
	}

	drawVbapSpace { | window, pos, rawDiameter, name, index |
		var satieVbapArea, spatializerName, posx, posy, diameter, objectText;
		diameter = rawDiameter * satieGuiScale;
		posx = satieGuiOrigin.x + pos.x;
		posy = satieGuiOrigin.y + pos.y;
		satieVbapArea = UserView.new(window,Rect(posx-(diameter/2), posy-(diameter/2),diameter,diameter));
		satieVbapArea.drawFunc = {
			Pen.strokeColor = Color.grey;
			Pen.strokeOval(Rect(0,0,diameter,diameter));
			Pen.line((diameter/2)-(diameter/20)@(diameter/2), (diameter/2)+(diameter/20)@(diameter/2));
			Pen.line((diameter/2)@((diameter/2)-(diameter/20)), (diameter/2)@((diameter/2)+(diameter/20)));
			Pen.stroke;
		};
	}

	drawHorizon { | window|
		window.drawFunc = {
			Pen.strokeColor = Color.grey;
			Pen.line(0@(satieGuiHeight/2), satieGuiWidth@(satieGuiHeight/2));
			Pen.stroke;
		};
	}

	drawSatieTopView {
		satieObjects[\windows].add(\satieGUITop -> Window.new("SATIE GUI - Top view", Rect(0,Window.screenBounds.height,satieGuiWidth,satieGuiHeight)));
		this.drawOrigin(satieObjects[\windows][\satieGUITop]);
		switch (satieObjects[\spatializers][\angles].size,
			0,   {"No spatilizers to draw".postln;},
			nil, {"nil: no spatilizers to draw".postln;},
			{
				"Spatializer(s) detected. Drawing...".postln;
				satieObjects[\spatializers][\angles].keysValuesDo { |key, value, spatializerIndex|
					this.drawVbapSpace(satieObjects[\windows][\satieGUITop], origin, satieObjects[\spatializers][\size][key.asSymbol], satieObjects[\spatializers][\angles].keys.asArray[spatializerIndex], spatializerIndex);
					if(satieObjects[\spatializers][\angles][key.asSymbol][0].isArray == false, // detect if spatializer is Vbap 2D
						{
							satieObjects[\spatializers][\angles][key.asSymbol].size.reverseDo{|i|
							var theta, position;
							theta = satieObjects[\spatializers][\angles][key.asSymbol][i].linlin(-180,180,270,-90);
							position = Point((theta.degrad.cos * (satieObjects[\spatializers][\size][key.asSymbol]/2)), (theta.degrad.sin * (satieObjects[\spatializers][\size][key.asSymbol]/2)));
							this.createSatieObject((key++i).asSymbol, satieObjects[\windows][\satieGUITop], position, satieObjectSize, i, satieObjects[\spatializers][\color][key.asSymbol]);
						}},{
							satieObjects[\spatializers][\angles][key.asSymbol].size.reverseDo {|i|
								var theta, thetaz, position;
								theta = satieObjects[\spatializers][\angles][key.asSymbol][i][0].linlin(-180,180,270,-90);
								thetaz = satieObjects[\spatializers][\angles][key.asSymbol][i][1];
								position = Point((theta.degrad.cos * (satieObjects[\spatializers][\size][key.asSymbol]/2)), (theta.degrad.sin * (satieObjects[\spatializers][\size][key.asSymbol]/2) * -1));
								position = position * thetaz.degrad.cos;
								this.createSatieObject((key++i++"Top").asSymbol, satieObjects[\windows][\satieGUITop], position, satieObjectSize, i, satieObjects[\spatializers][\color][key.asSymbol]);
							}
						}
					);
				};
				satieObjects.keysValuesDo { |key, value |
					if (satieObjects[\objects][key.asSymbol].class.asSymbol == \UserView, {
						satieObjects[\objects][key.asSymbol].mouseDownAction = {| view | view.visible = false};
						satieObjects[\objects][key.asSymbol].mouseUpAction = {| view | view.visible = true};
					});
				};
			}
		);
		this.drawLegends(satieObjects[\windows][\satieGUITop]);
		satieObjects[\windows][\satieGUITop].front;
		"Top view drawing done!".postln;
	}

	drawSatieFrontView {
		satieObjects[\windows].add(\satieGUIFront -> Window.new("SATIE GUI - Front view", Rect(0,Window.screenBounds.height,satieGuiWidth,satieGuiHeight)));
		this.drawOrigin(satieObjects[\windows][\satieGUIFront]);
		switch (satieObjects[\spatializers][\angles].size,
			0,   {"No spatilizers to draw".postln;},
			nil, {"nil: no spatilizers to draw".postln;},
			{
				"Spatializer(s) detected. Drawing...".postln;
				this.drawHorizon(satieObjects[\windows][\satieGUIFront]);
				satieObjects[\spatializers][\angles].keysValuesDo { |key, value, spatializerIndex|
					this.drawVbapSpace(satieObjects[\windows][\satieGUIFront], origin, satieObjects[\spatializers][\size][key.asSymbol], satieObjects[\spatializers][\angles].keys.asArray[spatializerIndex], spatializerIndex);
					if(satieObjects[\spatializers][\angles][key.asSymbol][0].isArray == false, // detect if spatializer is Vbap 2D
						{
							satieObjects[\spatializers][\angles][key.asSymbol].size.reverseDo {|i|
								var theta, position;
								theta = satieObjects[\spatializers][\angles][key.asSymbol][i].linlin(-180,180,270,-90);
								position = Point((theta.degrad.cos * (satieObjects[\spatializers][\size][key.asSymbol]/2)), 0);
								this.createSatieObject((key++i).asSymbol, satieObjects[\windows][\satieGUIFront], position, satieObjectSize, i, satieObjects[\spatializers][\color][key.asSymbol]);
							}
						},{
							satieObjects[\spatializers][\angles][key.asSymbol].size.do {|i|
								var theta, thetaz, position;
								theta = satieObjects[\spatializers][\angles][key.asSymbol][i][0].linlin(-180,180,270,-90);
								thetaz = satieObjects[\spatializers][\angles][key.asSymbol][i][1];
								position = Point((theta.degrad.cos * (satieObjects[\spatializers][\size][key.asSymbol]/2) * thetaz.degrad.cos), (thetaz.degrad.sin * (satieObjects[\spatializers][\size][key.asSymbol]/2) * -1));
								this.createSatieObject((key++i++"Front").asSymbol, satieObjects[\windows][\satieGUIFront], position, satieObjectSize, i, satieObjects[\spatializers][\color][key.asSymbol]);
							}
						}
					);
				};
				satieObjects.keysValuesDo { |key, value |
					if (satieObjects[\objects][key.asSymbol].class.asSymbol == \UserView, {
						satieObjects[\objects][key.asSymbol].mouseDownAction = {| view | view.visible = false};
						satieObjects[\objects][key.asSymbol].mouseUpAction = {| view | view.visible = true};
					});
				};
			}
		);
		this.drawLegends(satieObjects[\windows][\satieGUIFront]);
		satieObjects[\windows][\satieGUIFront].front;
		"Front view drawing done!".postln;
	}

	drawViews {
		this.drawSatieTopView;
		this.drawSatieFrontView;
	}

	// createSatieSources { | satiegui, sourcesColor, refreshFreq |
	// 	context.inspector.getAllSynths( { | synths |
	// 		synths.keysValuesDo({ | key, value, i |
	// 			var azi, ele, spherical, positionFront, positionTop;
	// 			azi = (value[\aziDeg].linlin(-180,180,270,-90))-180;
	// 			ele = value[\eleDeg];
	// 			spherical = Spherical.new((vbapSurfaceDiameter/2),((azi*pi)/180),((ele*pi)/180));
	// 			positionFront = Point.new(spherical.x, spherical.z);
	// 			positionTop = Point.new(spherical.x, spherical.y);
	// 			AppClock.sched(0.5,{ arg time;
	// 				satiegui.createSatieObject((key++"Top").asSymbol, satieObjects[\windows][\satieGUITop], positionTop, satieObjectSize, "", sourcesColor);
	// 				satiegui.createSatieObject((key++"Front").asSymbol, satieObjects[\windows][\satieGUIFront], positionFront, satieObjectSize, "", sourcesColor);
	// 			});

	// 			AppClock.sched(0.75,{ arg time;
	// 				context.inspector.getAllSynths( { | synths |
	// 					synths.keysValuesDo({ | key, value, i |
	// 						var azi, ele, spherical, positionFront, positionTop;
	// 						azi = (value[\aziDeg].linlin(-180,180,270,-90))-180;
	// 						ele = value[\eleDeg];
	// 						spherical = Spherical.new((vbapSurfaceDiameter/2),((azi*pi)/180),((ele*pi)/180));
	// 						positionFront = Point.new(spherical.x, spherical.z);
	// 						positionTop = Point.new(spherical.x, spherical.y);
	// 						satieObjects[\objectsPos].put((key++"Top").asSymbol, positionTop);
	// 						satieObjects[\objectsPos].put((key++"Front").asSymbol, positionFront);
	// 					});
	// 				});
	// 				//(0.1/refreshFreq);
	// 				0.1;
	// 			});

	// 			AppClock.sched(0.8,{ arg time;
	// 				satieObjects[\objects].keysValuesDo({ | key, value, i |
	// 					this.moveSatieObject(key.asSymbol, satieObjects[\objectsPos][(key++"Top").asSymbol].x, satieObjects[\objectsPos][(key++"Top").asSymbol].y);
	// 					this.moveSatieObject(key.asSymbol, satieObjects[\objectsPos][(key++"Front").asSymbol].x, satieObjects[\objectsPos][(key++"Front").asSymbol].y);
	// 					});
	// 				//(0.1/refreshFreq);
	// 				0.1;
	// 			});
	// 		});
	// 	});
	// }

}

