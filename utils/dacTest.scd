(
var window, levelMeters;
var config, model, view, presenter;

SynthDef(\dacTest, { |out, gate=1|
    var env = EnvGen.kr(Env.asr(0.03, 0.3, 0.01), gate, doneAction: Done.freeSelf);
    var sig = WhiteNoise.ar * env;
    Out.ar(out, sig)
}).add;

config = Environment(know: true).make({ |self|

    ~server = Server.default;
    ~wasBooted = self.server.serverRunning;
    ~maxChans = 64;
    ~numChans = 2;
	~taskChans = [0, 1];
    ~panDur = 1;

    ~spec = (
        channel: ControlSpec(1, self.maxChans, step: 1),
        volume: \db.asSpec,
        duration: ControlSpec(0.35, 1.5, step: 0.01),
    );

    ~boot = { |self|
        var numChans = self.numChans;
        self.server.options.numOutputBusChannels = numChans;
        self.taskChans = Array.iota(numChans);
        self.server.boot;
        self.changed(\serverBoot);
    };

    ~quit = { |self|
        self.server.quit;
        self.changed(\serverQuit);
    };

    ~reboot = { |self|
        var numChans = self.numChans;
        self.server.options.numOutputBusChannels = numChans;
        self.taskChans = Array.iota(numChans);
        self.changed(\serverReboot);
        self.server.reboot;
    };

});

model = Environment(know: true).make({ |self|

    ~synths = Array.newClear(config.maxChans);

    ~selected = OrderedIdentitySet[];

    ~selectIndex = { |self, index|
        self.selected = self.selected.add(index).sort;
    };

    ~deselectIndex = { |self, index|
        self.selected = self.selected.remove(index);
    };

	~task = Task({ 1.yield });
    ~watcher = SimpleController(self.task);

    ~taskRunning = false;

    ~makeTask = { |self|
        var task = Task({
            while { self.selected.size > 1 } {
                var array = self.selected.copy;
                self.changed(\activateButtons, array);
                array.do { |chan|
                    self.playSynth(chan);
                    self.changed(\updateGrid, chan, \green);
                    config.panDur.wait;
                    self.freeSynth(chan);
                    self.changed(\updateGrid, chan, \default);
                };
            };
        });

        self.watcher = SimpleController(task).put(\stopped, { self.stopTask });
        self.task = task;
    };

    ~runTask = { |self, value|
        switch(value)
        { 0 } { self.stopTask }
        { 1 } { self.playTask }
    };

    ~playTask = { |self|
        var channels;
        if(self.selected.isEmpty) {
            self.selected = OrderedIdentitySet.newFrom(config.taskChans);
        };
        channels = self.selected;
        self.resetTask;
        self.makeTask;
        self.changed(\activateButtons, channels);
        self.taskRunning = true;
        self.task.play(AppClock);
    };

    ~stopTask = { |self|
        self.selected.clear;
        self.taskRunning = false;
        self.resetTask(true);
        self.watcher.remove;
    };

    ~resetTask = { |self, hardReset=false|
        self.task.stop.reset;
        self.freeAllSynths;
        if(hardReset) { self.changed(\resetTask) };
    };

    ~freeAllSynths = { |self|
        self.synths.do { |synth, index|
            if(synth.notNil) { synth.release };
            self.synths[index] = nil;
        }
    };

    ~freeSynth = { |self, index|
        self.synths[index].release;
        self.synths[index] = nil;
    };

    ~playSynth = { |self, index|
        if(self.synths[index].isNil) {
            self.synths[index] = Synth(\dacTest, [\out, index]);
        }
    };

});

// GUI elements
view = Environment(know: true).make({ |self|

    ~emptySpace = 20; // pixels

    ~palette = (
        default: Button().palette.copy,
        green: Button().palette.copy.setColor(Color.green(1.25), \button),
    );

    ~startButton = Button()
    .maxWidth_(100)
    .states_([
        ["Boot"],
        ["Quit", Color.white, Color.gray],
    ])
    .action_({ |element|
        self.changed(\startButton, element.value);
    });

    ~rebootButton = Button()
    .maxWidth_(100)
    .enabled_(false)
    .states_([["Reboot"]])
    .action_({ |element|
        self.changed(\rebootButton)
    });

    ~panButton = Button()
    .maxWidth_(100)
    .enabled_(false)
    .states_([
        ["Start"],
        ["Stop", Color.white, Color.gray]
    ])
    .action_({ |element|
        self.changed(\panButton, element.value);
    });

    ~panicButton = Button()
    .states_([["Stop All Sound"]])
    .action_({ |element|
        self.changed(\panicButton);
    });

    ~meterButton = Button()
    .enabled_(false)
    .states_([
        ["Level Meters"],
        ["Level Meters", Color.white, Color.gray]
    ])
    .action_({ |element|
        self.changed(\meterButton, element.value);
    });

    ~muteButton = Button()
    .maxWidth_(50)
    .states_([["Mute"], ["Mute", Color.white, Color.red]])
    .action_({ |element|
        self.changed(\serverMute, element.value);
    });

    ~channelSlider = Slider()
    .orientation_(\horizontal)
    .action_({ |element|
        self.changed(\channelSlider, element.value);
    });

    ~durationSlider = Slider()
    .orientation_(\horizontal)
    .action_({ |element|
        self.changed(\durationSlider, element.value);
    });

    ~volumeSlider = Slider()
    .orientation_(\horizontal)
    .action_({ |element|
        self.changed(\volumeSlider, element.value);
    });

    ~channelBox = NumberBox()
    .fixedWidth_(40)
    .action_({ |element|
        self.changed(\channelBox, element.value)
    });

    ~durationBox = NumberBox()
    .fixedWidth_(40)
    .action_({ |element|
        self.changed(\durationBox, element.value)
    });

    ~volumeBox = NumberBox()
    .fixedWidth_(40)
    .action_({ |element|
        self.changed(\volumeBox, element.value)
    });

    ~gridButtons = Array.fill(
        size: config.maxChans,
        function: { |index|
            Button()
            .enabled_(false)
            .fixedWidth_(40)
            .states_([
                [index],
                [index, Color.white, Color.gray],
            ])
            .action_({ |element|
                self.changed(\gridButtons, element.value, index)
            })
        }
    );

    ~activateButtons = { |self, indexes|
        var tmp = config.taskChans.copy.removeAll(indexes);
        tmp.do { |i|
            self.gridButtons[i].value_(0);
        };
        indexes.do { |i|
            self.gridButtons[i].value_(1);
        }
    };

    ~resetButtons = { |self|
        self.gridButtons.do { |button|
            button.palette_(view.palette.default);
            button.value_(0);
        }
    };

    ~enableButtons = { |self, num|
        var grid = self.gridButtons;
        grid.do { |button| button.enabled_(false) };
        if(num > 0) {
            num.do { |i| grid[i].enabled_(true) }
        }
    };

    ~openLevelMeters = { |self, value|
        if(levelMeters.notNil) {
            levelMeters.window.close;
            levelMeters = nil;
        };
        if(value === 1) {
            levelMeters = ServerMeter(config.server);
            view.meterButton.value_(1);
        };
    };

});

// create a presenter that is a dependant of config, model, and view
presenter = SimpleController(model);
view.addDependant(presenter);
config.addDependant(presenter);

// config.changed actions
(
    serverBoot: {
        view.enableButtons(config.numChans);
        view.rebootButton.enabled_(true);
        view.meterButton.enabled_(true);
        view.panButton.enabled_(true);
    },

    serverQuit: {
        model.resetTask(true);
        view.enableButtons(0);
        view.rebootButton.enabled_(false);
        view.meterButton.valueAction_(0);
        view.meterButton.enabled_(false);
        view.panButton.enabled_(false);
    },

    serverReboot: {
        model.resetTask(true);
        view.enableButtons(config.numChans);
        if(levelMeters.notNil) {
            view.meterButton.valueAction_(1);
        } {
            view.meterButton.value_(0);
        };
    },

).keysValuesDo { |key, action|
    presenter.put(key, action)
};

// model.changed actions
(
    resetTask: {
        view.resetButtons;
        view.panButton.value_(0);
    },

    activateButtons: { |who, what, indexes|
        view.activateButtons(indexes);
    },

    updateGrid: { |who, what, index, color|
        var button = view.gridButtons[index];
        switch(color)
        { \green } {
            button.palette_(view.palette.green);
            button.value_(0);
        }
        { \default } {
            button.palette_(view.palette.default);
            button.value_(1);
        };
    },

).keysValuesDo { |key, action|
    presenter.put(key, action)
};

// view.changed actions
(
    startButton: { |who, what, value|
        switch(value)
        { 0 } {
            if(config.server.serverRunning) {
                config.quit;
            }
        }
        { 1 } {
            if(config.server.serverRunning.not) {
                config.boot;
            }
        }
    },

    rebootButton: {
        if(config.server.serverRunning) {
            config.reboot;
        }
    },

    panicButton: {
        model.resetTask(true);
    },

    meterButton: { |who, what, value|
        view.openLevelMeters(value);
    },

    serverMute: { |who, what, value|
        switch(value)
        { 0 } { config.server.unmute }
        { 1 } { config.server.mute }
    },

    channelSlider: { |who, what, value|
        config.numChans = config.spec.channel.map(value).asInteger;
        view.channelBox.value_(config.numChans);
    },

    channelBox: { |who, what, value|
        view.channelSlider.valueAction_(config.spec.channel.unmap(value));
    },

    panButton: { |who, what, value|
        model.runTask(value);
    },

    durationSlider: { |who, what, value|
        config.panDur = config.spec.duration.map(value);
        view.durationBox.value_(config.panDur);
    },

    durationBox: { |who, what, value|
        view.durationSlider.valueAction_(config.spec.duration.unmap(value));
    },

    volumeSlider: { |who, what, value|
        var volume = config.spec.volume.map(value);
        config.server.volume_(volume);
        view.volumeBox.value_(volume);
    },

    volumeBox: { |who, what, value|
        view.volumeSlider.valueAction_(config.spec.volume.unmap(value));
    },

    gridButtons: { |who, what, value, index|
        if(config.server.serverRunning.not) {
            view.gridButtons[value].value_(0)
        } {
            switch(value)
            { 0 } {
                if(model.taskRunning.not) { model.freeSynth(index) };
                model.deselectIndex(index);
            }
            { 1 } {
                if(model.taskRunning.not) { model.playSynth(index) };
                model.selectIndex(index);
            }
        }
    },

).keysValuesDo { |key, action|
    presenter.put(key, action)
};

// Window layout
window = Window("DAC Tester")
.layout_(
    VLayout(
        VLayout(
            HLayout(
                StaticText().string_("Default Server"),
                view.startButton,
                view.rebootButton,
                view.meterButton,
            ),
            HLayout(
                StaticText().string_("Output Channels"),
                view.channelSlider,
                view.channelBox,
            ),
            view.emptySpace,
        ),
        VLayout(
            HLayout(
                StaticText().string_("Volume"),
                view.muteButton,
                view.volumeSlider,
                view.volumeBox,
            ),
            view.panicButton,
            view.emptySpace,
        ),
        VLayout(
            HLayout(
                StaticText().string_("Cycle Channels"),
                view.panButton,
            ),
            HLayout(
                StaticText().string_("Duration"),
                view.durationSlider,
                view.durationBox,
            ),
            view.emptySpace,
        ),
        VLayout(
            StaticText().string_("Output Channel Indexes"),
            GridLayout.rows(*view.gridButtons.clump(8)),
        )
    )
);

window.onClose_({
    model.resetTask;
    if(config.wasBooted.not && config.server.serverRunning) { config.server.quit };
    model.releaseDependants;
    view.releaseDependants;
    config.releaseDependants;
});

// enable/disable some GUI elements if Server.default was already booted
if(config.server.serverRunning) {
    var numOutputs = config.server.options.numOutputBusChannels;
    config.wasBooted = true;
    view.channelSlider.valueAction_(config.spec.channel.unmap(numOutputs));
    view.enableButtons(numOutputs);
    view.meterButton.enabled_(true);
    view.panButton.enabled_(true);
    view.startButton.enabled_(false);
    view.rebootButton.enabled_(false);
    view.channelSlider.enabled_(false);
};

// init slider values
view.channelSlider.valueAction_(config.spec.channel.unmap(config.numChans));
view.durationSlider.valueAction_(config.spec.duration.unmap(config.panDur));
view.volumeSlider.valueAction_(config.spec.volume.unmap(config.server.volume.volume));

// open Window
window.front;
)
