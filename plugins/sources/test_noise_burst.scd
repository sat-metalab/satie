// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

~name = \test_noise_burst;
~description = "A short burst of enveloped noise that goes well with Patterns";
~channelLayout = \mono;

~function = { arg amp=1, trimDB = 0, attack = 0.01, release = 0.3;
	var env;
	env = EnvGen.ar(Env.perc(attack, release, 0.5), doneAction: Done.freeSelf);
	trimDB.dbamp*amp*PinkNoise.ar(1);
};
