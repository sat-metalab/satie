// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

// SynthDef by Victor Comby, original idea by Eli Fieldsteel

~name = \Marimba;
~description = "Marimba-like, random pitch";
~channelLayout = \mono;
~function = { |pitch = 880, maxPartial = 4|
	var sig, rate, freq;
	freq = LFNoise1.kr(4).exprange(pitch, pitch*maxPartial).round(pitch);
	rate = LFNoise1.ar(2).range(2, 6);
	sig = Saw.ar(rate);
	sig = BPF.ar(sig, freq, 0.005);
};


~setup = {};