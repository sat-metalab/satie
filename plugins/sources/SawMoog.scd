// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

// based on tweet0355 by redFrik

/*       Audio plugin definition

	Each audio plugin should define the following global variables:
	~name: (symbol) name of the spatializer
	~description: (string) a short description
	~function: the actual definition of the plugin

*/

~name = \SawMoog;
~description = "Saw synths through a Moog filter";
~channelLayout = \mono;

~function = {| gain = 3.5, freq = 7, amp = 0.5|
	  var snd, noiz, out;
	  var filtfreq;
	  noiz = GrayNoise.ar(999);
	  snd = LFSaw.ar(freq, 3/freq);
	  snd = LFSaw.ar(snd)>0.05;
	  snd = snd * noiz * LFSaw.ar() * 0.05;
	  filtfreq = 2**LFSaw.ar(freq/3.55).round;
	  filtfreq = filtfreq * freq/3.55 * 99;
	  out = MoogFF.ar(snd, filtfreq, gain, mul: amp);
	  Clipper8.ar(out, -0.3, 0.3);
};
