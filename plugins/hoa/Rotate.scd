// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.


~name = \Rotate;
~description = "Rotate the sound field (args in degree from -180 to 180)";

~function = { |in = 0, order = 1, rotatePitchDeg = 0, rotateRollDeg = 0, rotateYawDeg = 0|

	// Rotating the scene is done from the perspective of the listener
	// rotation follows the right-hand rule of rotation:
	//   * Positive pitch means the listener tilts head upwards
	//   * Positive roll means the listener tilts head to the right
	//   * Positive yaw means the listener turns head to the left

	HOATransRotateXYZ.ar(
		order: order,
		in: in,
		pitch: rotatePitchDeg.degrad,
		roll: rotateRollDeg.degrad,
		yaw: rotateYawDeg.degrad
	);
};
