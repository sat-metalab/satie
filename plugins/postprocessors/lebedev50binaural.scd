// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

~name = \lebedev50binaural;
~description = "A 50 channel Lebedev grid decoded to binaural";
~channelLayout = \mono;

~function = { |in=0|
	var sig = Lebedev50BinauralDecoder.ar(in);
	// Lebedev50BinauralDecoder has an inverted azimuth
	// swap the signals so that +90 degrees is heard on the right
	[sig[1], sig[0]]
};

~setup = { |satie|
	Lebedev50BinauralDecoder.loadHrirFilters(satie.config.server);
};

