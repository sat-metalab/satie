// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

/*       Spatializer plugin definition

	Each spatializer plugin should define the following global variables:
	~name: (symbol) name of the spatializer
	~description: (string) a short description
	~numChannels: (int) number of channels
	~channelLayout: (symbol) layout for each channel, usually mono or ambi
	~angles: Speaker array angles, if using VBAP call VBAPSpeakerArray
	~function: the definition of the spatializer

	where function should use the following input arguments:

	in
	aziDeg (+/- 180 degrees) default = 0
	elevDeg (+/- 90 degrees) default = 0
	gainDB (dB) default = -99
	delayMs (milliseconds) default = 1
	lpHz (Hz) default = 15000
	hpHz (Hz) default = 5
	spread (range 0-1) default = 0.01
*/

~name = \lebedev50VBAP;
~description = "A 50 channel Lebedev grid as VBAP";
~numChannels = 50;
~channelLayout = \mono;
~angles = nil;

~function = { |in = 0, aziDeg = 0, eleDeg = 45, gainDB = -99, delayMs = 1, lpHz = 15000, hpHz = 5, spread = 0.2|

	var gain = gainDB.dbamp;       // convert gainDB to gainAMP
	var delay = delayMs * 0.001;   // convert to seconds
	var slewDelay = 0.3;           // note: this needs to be improved ... smoother
	var slewGain = 0.1;
	var slewFilter = 0.3;
	var slewPanning = 0.03;
	var outsig;

	// limit cutoff freq range and smooth changes
	lpHz = lpHz.clip(5.0, 20000.0).lag3(slewFilter);
	hpHz = hpHz.clip(5.0, 20000.0).lag3(slewFilter);

	outsig = in * gain.lag(slewGain);
	outsig = DelayC.ar(
		outsig,
		maxdelaytime: 0.5,
		delaytime: delay.lag(slewDelay)
	);
	outsig = LPF.ar(outsig, lpHz);
	outsig = BHiPass.ar(outsig, hpHz);

	VBAP.ar(
		numChans: 50,
		in: outsig,
		bufnum: SatieFactory.lebedev50buffer,
		azimuth: aziDeg.circleRamp(slewPanning),
		elevation: eleDeg.circleRamp(slewPanning),
		spread: spread * 100.0, // default spread is 20
	);
};

