// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

/*  Spatializer plugin definition

	Each spatializer plugin should define the following global variables:
	~name: (symbol) name of the spatializer
	~description: (string) a short description
	~numChannels: (int) number of channels
	~function: the definition of the spatializer

	where function should use the following input arguments:

	in
	aziDeg +/- 180 degrees
	elevDeg +/- 90 degrees
	gainDB  decibels
	delaySec  seconds
	lpHz    hertz
	spread (range 0-1) default = 0.01
*/

~name = \dodecahedronLeslie;
~description = "Dodecahedron Leslie";
~numChannels = 12;
~channelLayout = \mono;

~function = { |in = 0, aziDeg = 0, eleDeg = 45, gainDB = -99, delayMs = 1, lpHz = 15000, hpHz = 5, spread = 0.01, rotationSpeed = 5|

	var gain= gainDB.dbamp;        // convert gainDB to gainAMP
	var delay = delayMs * 0.001;   // convert to seconds
	var slewDelay = 0.3;           //  note: this needs to be improved ... smoother
	var slewGain = 0.1;
	var slewFilter = 0.3;
	var slewPanning = 0.03;
	var outsig;

	// limit cutoff freq range and smooth changes
	lpHz = lpHz.clip(5.0, 20000.0).lag3(slewFilter);
	hpHz = hpHz.clip(5.0, 20000.0).lag3(slewFilter);

	outsig = in * gain.lag(slewGain);
	outsig = DelayC.ar(
		outsig,
		maxdelaytime: 0.5,
		delaytime: delay.lag(slewDelay)
	);
	outsig = LPF.ar(outsig, lpHz);
	outsig = BHiPass.ar(outsig, hpHz);

	[0, 0, 0,
	outsig*EnvGen.kr(Env.sine(1/(3*rotationSpeed)), Impulse.kr(rotationSpeed, (1/6))),
	outsig*EnvGen.kr(Env.sine(1/(3*rotationSpeed)), Impulse.kr(rotationSpeed, (2/6))),
	outsig*EnvGen.kr(Env.sine(1/(3*rotationSpeed)), Impulse.kr(rotationSpeed, (3/6))),
	outsig*EnvGen.kr(Env.sine(1/(3*rotationSpeed)), Impulse.kr(rotationSpeed, (4/6))),
	outsig*EnvGen.kr(Env.sine(1/(3*rotationSpeed)), Impulse.kr(rotationSpeed, (5/6))),
	outsig*EnvGen.kr(Env.sine(1/(3*rotationSpeed)), Impulse.kr(rotationSpeed, (6/6))),
	0, 0, 0];
};
