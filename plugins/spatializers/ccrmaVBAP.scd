// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

/*       Spatializer plugin definition

	Each spatializer plugin should define the following global variables:
	~name: (symbol) name of the spatializer
	~description: (string) a short description
	~numChannels: (int) number of channels
	~channelLayout: (symbol) layout for each channel, usually mono or ambi
	~angles: Speaker array angles, if using VBAP call VBAPSpeakerArray
	~function: the definition of the spatializer

	where function should use the following input arguments:

	in
	aziDeg +/- 180 degrees
	elevDeg +/- 90 degrees
	gainDB  decibels
	delaySec  seconds
	lpHz    hertz
	spread (range 0-1) default = 0.01
*/

// CCRMA stage 56-channel VBAP
// https://ccrma.stanford.edu/room-guides/ccrma-stage

~name = \ccrmaVBAP;
~description = "56 channel speaker layout on a dome (CCRMA)";
~numChannels = 56;
~channelLayout = \mono; 

VBAPSpeakerArray.maxNumSpeakers = 56;

~angles = ~spkCCRMA_56 = VBAPSpeakerArray.new(3, [ // make array globally available after loading the spatializer

	// towers 8:
	[27, 3.9],
	[-27, 3.9],
	[63, 8],
	[-63, 8],
	[117, 8],
	[-117, 8],
	[153, 3.9],
	[-153, 3.9],
	// upper 8
	[23,  29],
	[-23,  29],
	[90,  58],
	[-90,  58],
	[157,  31],
	[-157,  31],
	[0,  70],
	[180,  70],
	// ring of 12 (rails)
	[9,  4 ],
	[-9,  4 ],
	[45,  6 ],
	[-45,  6 ],
	[81,  8 ],
	[-81,  8 ],
	[99,  8 ],
	[-99,  8 ],
	[135,  6 ],
	[-135,  6 ],
	[171,  4 ],
	[-171,  4 ],
	// ring of 14 (lower trusses)
	[14,  18],
	[-14,  18],
	[39,  22],
	[-39,  22],
	[60,  30],
	[-60,  30],
	[90,  34],
	[-90,  34],
	[122,  30],
	[-122,  30],
	[144,  22],
	[-144,  22],
	[166,  19],
	[-166,  19],
	// ring of 6 (upper trusses)
	[0,  31],
	[39,  47],
	[-39,  47],
	[146,  47],
	[-146,  4],
	[180,  33],
	// lower ring of 8 in towers
	[27,   -10],
	[-27,   -10],
	[63,   -14],
	[-63,   -14],
	[117,   -14],
	[-117,   -14],
	[153,   -10],
	[-153,   -10]
]);

~function = { |in = 0, aziDeg = 0, eleDeg = 45, gainDB = -99, delayMs = 1, lpHz = 15000, hpHz = 5, spread = 0.01|

	var gain = gainDB.dbamp;       // convert gainDB to gainAMP
	var delay = delayMs * 0.001;   // convert to seconds
	var slewDelay = 0.3;           // note: this needs to be improved ... smoother
	var slewGain = 0.1;
	var slewFilter = 0.3;
	var slewPanning = 0.03;
	var outsig;

	// limit cutoff freq range and smooth changes
	lpHz = lpHz.clip(5.0, 20000.0).lag3(slewFilter);
	hpHz = hpHz.clip(5.0, 20000.0).lag3(slewFilter);

	outsig = in * gain.lag(slewGain);
	outsig = DelayC.ar(
		outsig,
		maxdelaytime: 0.5,
		delaytime: delay.lag(slewDelay)
	);
	outsig = LPF.ar(outsig, lpHz);
	outsig = BHiPass.ar(outsig, hpHz);

	VBAP.ar(
		numChans: ~spkCCRMA_56.numSpeakers,
		in: outsig,
		bufnum: ~vbufCCRMA_56.bufnum,
		azimuth: aziDeg.circleRamp(slewPanning),
		elevation: eleDeg.circleRamp(slewPanning),
		spread: spread * 100
	);

};

~setup = { |satieInstance|
		~vbufCCRMA_56 = Buffer.loadCollection(satieInstance.config.server, ~spkCCRMA_56.getSetsAndMatrices);
};

/*
OFFICIAL info received from CCRMA:

First column is azimuth, second is elevation (both in degrees) and third is distance to the center of the Stage (in inches):


$ more speakers_stage2017.m
function [ val ] = speakers_stage2017( )
%% Stage 2017 GRAIL test, ambisonics coordinates
%% (positive angle = counterclockwise)

   val = ambi_mat2spkr_array( ...
       [
	%% == towers 8:
	%% theoretical angles, have to be calibrated
	27     3.9   216;
	-27    3.9   216;
	63     8   162;
	-63    8   162;
	117    8   162;
	-117   8   162;
	153    3.9   216;
	-153   3.9   216;

	%% == upper 8
	23     29  171;
	-23    29  171;
	90     58  109;
	-90    58  109;
	157    31  167;
	-157   31  167;
	0      70  108;
	180    70  108;

	%% == ring of 12 (rails)
	9      4   237;
	-9     4   237;
	45     6   187;
	-45    6   187;
	81     8   131;
	-81    8   131;
	99     8   130;
	-99    8   130;
	135    6   185;
	-135   6   185;
	171    4   238;
	-171   4   238;

	%% == ring of 14 (lower trusses)
	14     18  243;
	-14    18  243;
	39     22  200;
	-39    22  200;
	60     30  154;
	-60    30  154;
	90     34  139;
	-90    34  139;
	122    30  153;
	-122   30  153;
	144    22  201;
	-144   22  201;
	166    19  243;
	-166   19  243;

	%% == ring of 6 (upper trusses)
	0      31  180;
	39     47  128;
	-39    47  128;
	146    47  129;
	-146    47  129;
	180    33  180;

	%% == lower ring of 8 in towers
	27     -10   216;
	-27    -10   216;
	63     -14   162;
	-63    -14   162;
	117    -14   162;
	-117   -14   162;
	153    -10   216;
	-153   -10   216;
       ], ...
       'AER', ...
       'DDI', ...
	'amb', ...
	{
	 'S01'; 'S02'; 'S03'; 'S04';
	 'S05'; 'S06'; 'S07'; 'S08';
	 'S09'; 'S10'; 'S11'; 'S12';
	 'S13'; 'S14'; 'S15'; 'S16';
	 'D17'; 'D18'; 'D19'; 'D20'; 'D21'; 'D22';
	 'D23'; 'D24'; 'D25'; 'D26'; 'D27'; 'D28';
	 'D29'; 'D30'; 'D31'; 'D32'; 'D33'; 'D34'; 'D35';
	 'D36'; 'D37'; 'D38'; 'D39'; 'D40'; 'D41'; 'D42';
	 'D43'; 'D44'; 'D45'; 'D46'; 'D47'; 'D48';
	 'L01'; 'L02'; 'L03'; 'L04';
	 'L05'; 'L06'; 'L07'; 'L08';

	});

*/
/*
	// testing:

(
	s = Server.supernova.local;
	~satieConfiguration = SatieConfiguration.new(s, [\ccrmaVBAP]);
	~satie = Satie.new(~satieConfiguration);
	~satie.boot();
	s.waitForBoot({
    // display some information
    s.meter;
    // s.makeGui;
	//    s.plotTree;
	})
)


*/
