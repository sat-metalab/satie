---
title: SATIE OSC protocol API
authors:
  - Michał Seta
  - Patrick Dupuis
...

# SATIE OSC protocol API

## SATIE concepts overview

An OSC message in SATIE always starts with an OSC address that includes a command (a verb) as its last component, followed by zero or more parameters. OSC messages use semantics that reflect SATIE's "world" architecture, where SATIE is a container for a `scene`, which is a container for a `group` which is a container for a `node`.

ex.: /`satie`/`scene`/`deleteNode` `<node name>`


```
   +-------------------------------------------+
   |  SATIE                                    |
   |                                           |
   |+-----------------------------------------+|
   ||  Scene                                  ||
   ||                                         ||
   ||                                         ||
   ||+---------------------------------------+||
   |||  Group                                |||
   |||                                       |||
   |||              +-----------------+      |||
   |||              |  Node (source)  |      |||
   |||              |       (effect)  |      |||
   |||              |       (process) |      |||
   |||              |                 |      |||
   |||              |                 |      |||
   |||              |                 |      |||
   |||              |                 |      |||
   |||              |                 |      |||
   |||              +-----------------+      |||
   |||                                       |||
   |||                                       |||
   |||                                       |||
   ||+---------------------------------------+||
   |+-----------------------------------------+|
   +-------------------------------------------+
```

`SATIE` level will usually take messages addressing configuration. At this time only loading of a SuperCollider source file is possible.
`Scene` is the place where one adds and removes `nodes`.

As can be seen in the above diagram, there are three types of `node`:

- `Source` (or generator), in other words, an instrument. A source node can produce an audio signal.
- `Effect`, needs a signal in its input in order to generate some output.
- `Process`. It is explained in the documentation of the SATIE quark, its role and functionality is too complex for the scope of this introduction.

The general workflow is:

- creating nodes
- interacting with nodes

### Groups

Groups in SATIE play two-fold role:

- ease of addressing many nodes at the same time (with one OSC message)
- ordering of signal chain:


```
   +------------+   +---------+   +----------------------+   +-----------------+
   | generators |-->| effects |-->| ambi-post-processors |-->| post-processors |
   +------------+   +---------+   +----------------------+   +-----------------+

```

In fact, SATIE creates default groups for those 4 types of nodes with default names:

- `default` for generators
- `defaultFX` for effects
- `ambiPostProc` for ambisonic post-processor
- `postProc` for non-ambisonic post-processors

The user can create an arbitrary number of generators and effects groups.
The post-processors groups are singletons. The former groups can be created either explicitly, via `create*` command (see the details below), or while instantiating nodes and specifying the `group` parameter.
In this case, if the group of the given name does not exist, it will be created automatically.

Process groups fall in the _generators_ category.

## SATIE messages

`/satie/configure json_string`

Configure SATIE using stringified JSON.

- json_string: A JSON string containing a SATIE configuration. See example in documentation for Class _SatieConfiguration_.

For example:

```
{
  "satie_configuration": {
    "server": {
      "name": null,
      "supernova": false
    },
    "minOutputBusChannels": 2,
    "numAudioAux": 2,
    "ambiOrders": [ 1, 3 ],
    "listeningFormat": [ "stereoListener", "domeVBAP" ],
    "outBusIndex": [ 0, 2 ],
    "userPluginsDir": nil
  }
}
```

`satie/configuration/get`

Get current SATIE configuration. SATIE will respond with a message to `/satie/configuration` containing a JSON string:

```
{ "listeningFormat": [ "stereoListener" ], "outBusIndex": [ 0 ], "numAudioAux": 0, "server": SatieJson, "ambiOrders": [  ], "minOutputBusChannels": 2 }
```


`/satie/boot`

Boots SATIE and its server.

`/satie/quit`

Quits SATIE and its server.

`/satie/reboot`

Reboots SATIE and its server. If SATIE is not booted when this message is received, SATIE will simply boot.

`/satie/load filename`

Load and evaluate a file in supercollider. Filename must be a full file path to a file located on the audio rendering machine

`/satie/loadSample name path`

Load an audio sample into Satie's audioSamples dictionary. Once loaded, an audio sample's name can be used as a substitute
to providing the internal buffer number for that sample when creating source/effect/process nodes.

- name: the chosen name for the audio sample
- path: the path where the audio sample is located

Example loadSample OSC message:

`/satie/loadSample mySample ~/audio/mySample.wav`

Example createSource OSC message using sample 'mySample':

`/satie/scene/createSource mySource sndBuffer default bufnum mySample gate 1 gainDB -20`

`/satie/server/options optionName`

Ask SATIE to report the value of the option contained in its `ServerOptions` instance.

SATIE will with an OSC message repeating the option name and its value:

`/satie/server/option optionName value`

See SC's `ServerOptions` documentation for the list of possible options to inquire about.

## Scene Messages

A "scene" contains source/effect/process nodes which are kept in groups.
These messages create and manage scene content.


`/satie/scene/createSource nodeName synthdefName \<groupName\> \<key1 value1 ... keyN valueN\>`

Instantiate an audio generator plugin.

- nodeName : the name of the instance.
- synthDefName : the name of the synthdef registered with the system.
- groupName : (optional) the name of the group. Default: '\\default'
- key, value : (optional) synth arguments as key value pairs

<pre class=note>
<span class=note text>NOTE:<span> `groupName` parameter becomes required if `key/value` pairs for synth parameters are provided
</pre>

`/satie/scene/createKamikaze synthdefName \<groupName\> \<key1 value1 ... keyN valueN\>`

Create a special version of an audio generator plugin ("Kamikaze") that will stop itself when it becomes silent.
Note that Kamikaze instances cannot be controlled individually since the Kamikaze instance has not been named.

- synthDefName : the name of the synthdef registered with the system.
- groupName : (optional) the name of the group. Default: '\\default'
- key, value : (optional) synth arguments as key value pairs

<pre class=note>
<span class=note text>NOTE:<span> `groupName` parameter becomes required if `key/value` pairs for synth parameters are provided
</pre>


`/satie/scene/createEffect nodeName synthdefName \<groupName\> \<auxBusIndex\> \<key1 value1 ... keyN valueN\>`

Instantiate an audio effect plugin

- nodeName : the name of the instance.
- synthDefName : the name of the synthdef registered with the system.
- groupName : (optional) the name of the group. Default: '\defaultFx'
- auxBusIndex: (optional) number of aux bus for the effect's input. Default: '0'
- key, value : (optional) synth arguments as key value pairs

<pre class=note>
<span class=note text>NOTE:<span> `groupName` and `auxBusIndex` parameters become required if `key/value` pairs for synth parameters are provided
</pre>


`/satie/scene/createProcess id processName \<key1 value1 ... keyN valueN\>`

Create/instantiate a `process`

-  id : the name given to the process instance that will be created
-  processName : the name of the process definition to be used
-  key, value : (optional) process properties as key value pairs

`/satie/scene/createSourceGroup nodeName`

Create a new group at the head of the Satie's DSP chain

-  nodeName : name of the group.

`/satie/scene/createEffectGroup nodeName`

Create a new group at the end of Satie's DSP chain

-  nodeName : name of the group.

`/satie/scene/createProcessGroup nodeName`

Create a new group on the head of the DSP chain

-  nodeName : name of the group.

`/satie/scene/deleteNode nodeName`

Delete a source, effect, process, or group

-  nodeName : the name of the node to be deleted

`/satie/scene/clear`

Clear the scene. Removes all instances and groups.

`/satie/scene/debug debugFlag`

   Enable or disable debug printing
-  debugFlag : 1 or 0, defaults to 0

### Post-processor messages
Post-processors creation (i.e. methods `replacePostProcessor` and `replaceAmbiPostProcessor`) are handled in two steps.
First, we set their properties and then we apply the method.

`/satie/scene/postproc/set key value`

Set one property at a time. Valid only for properties that use only one value (i.e. `outputIndex`)

`/satie/scene/postproc/setarray key value1 value2 ... valueN`

Set a property that requires a list of properties. Since SuperCollider does not handle arrays in OSC, we send a sequence of desired values

`/satie/scene/postproc/apply`

Execute `replacePostProcessor` method (will use previously set values as arguments)

`/satie/scene/ambipostproc/set key value`

Same as above version for `postproc`

`/satie/scene/ambipostproc/setarray key value1 value2 ... valueN`

Same as above version for `postproc`

`/satie/scene/ambipostproc/apply`

Execute `replaceAmbiPostProcessor` method with appropriate arguments

`/satie/scene/postproc/prop/set name key1 value1 ... keyN valueN`

Change postprocessor properties. Note, that the same message works for `satie.postProcessor` and `satie.ambiPostProcessor`, `name` will serve to select the processor to act on:

- name: the instance name of the post-process
- key, val: property and new value. Several key-value pairs can be sent in one message.

## Node messages

Nodes are instances of audio sources, effects or processes.
Nodes also belong to groups.
There are three ways of addressing a node:

-   source - individual instance of type `source` or `effect`
-   group
-   process

`/satie/\<nodeType\>/state nodeName value`

Node state (whether it is playing/computing or not): 1 = active, 0 = inactive

`/satie/\<nodeType\>/set nodeName key1 val1 ... keyN valN`

Set the values of one or more properties of a source, group, or process.

- nodeName : the name of the source, group, or process instance
- key, value : propertie names and values

`/satie/\<nodeType\>/setvec nodeName key val1 ... valN`

Set a vector

<pre class=note>
<span class=note text>NOTE:<span> `set` and `setvec` messages are used to address specific properties.
Each node has two groups of properties: those provided by the spatializer and those specific to the node. The latter vary from node to node.
</pre>
Properties common to all:

- preBus\_gainDB (dB) default = 0
- postBus\_gainDB (dB) default = 0

Spatializer properties (contained in most spatializers)

- aziDeg (+/- 180 degrees) default = 0
- eleDeg (+/- 90 degrees) default = 0
- gainDB (dB) default = -99
- delayMs (milliseconds) default = 1
- lpHz (Hz) default = 15000
- hpHz (Hz) default = 5
- spread (range 0-1) default = 0.01

## Only for nodeTypes: source and process

`/satie/\<nodeType\>/update nodeName azimuth elevation gainDB delayMS lpHz`

Update some essential properties at once.
This message is typically sent every frame, all properties relate to node's position.
This method is a shorthand to sending many `set` messages, behind the scenes it simply weaves received values to corresponding keys.

By default, the update message expects the following values:

-  nodeName : name of the node
-  azimuth : azimuth in degrees (+/- 180)
-  elevation : elevation in degrees (+/- 90)
-  gainDB : gain in decibels
-  delayMS : delay in milliseconds
-  lpHz : low pass filter in Hertz

The content of the `update` message is configurable. The variable `SatieOSC.update_custom_keys` is a `List` (by default it is empty) to hold extra _keys_. You can assign a list of any parameters affecting SATIE instances and it will forward appropriate messages, i.e:

```
~satie.osc.update_custom_keys = [\freq]
```

will tell SATIE to expect 5 values.
Sending `/satie/source/update sffff boo 90 0 -30 1.0 12000 800` will weave the _keys_ with received _values_ to `\aziDeg, 90, \eleDeg, 90, \gainDB, -30, \delayMS, 1.0, \lpHz, 12000, \freq, 800` and set these parameters to the instance named `boo` .

The message is checked for length so SATIE will print a warning if the number of received values is not equal to the sum of lengths of `Satie.update_message_keys` and `Satie.update_custom_keys` but will try to apply the message anyways.

<pre class=note>
<span class=note text>NOTE:<span> This behavior affects how `update` is handled by _Process_ that implements `setUpdate` method. This method should not assume that the `update` message is composed of values corresponding to `aziDeg, eleDeg, gainDB, delayMS, lpHz, distance` anymore. In the current implementation `setUpdate`'s signature should be as follows `setUpdate = { |self, aziDeg, eleDeg, gainDB ... args| }` where `args` will be provided as a list of values directly forwarded from the OSC message.
</pre>

### Only for noteType: process

`/satie/process/property processName key1 value1 ... keyN valueN`

<pre class=note>
<span class=note text>NOTE:<span> This exists for backwards compatibility. Use `/satie/process/set` in new projects.
</pre>

Update several process environment properties.
Works only if the process implements a function named `property` which takes a single key-value pair.
The process' `property` function will be called once for each key-value pair provided in the OSC message.

- processName : the name of the process instance
- key, value : process property names and values

`/satie/process/eval processName handlerName optArg1 ... opeArgN`

Invoke a process function with zero or more arguments

- eval handlerName, args\[\]

## Introspection

You can query SATIE via OSC and get some information. SATIE responds to the following messages:

`/satie/plugins`

get existing synthdefs

SATIE responds with an osc message: /plugins JSON string containing 3 objects: generators, effects and mastering, each being a JSON object name: {type, description} (where name = the "id" passed to Satie::makeSynthDef, type = SatiePlugin

Example output (line wrapped for readability):
``` javascript
{ "generators":
  { "mybuffer": { "description": "Play a buffer", "type": "sndBuffer"}
}}
```

`/satie/pluginargs synthdefName`

get arguments of some synthdef

- synthdefName string

SATIE responds with an osc message: /arguments JSON string representing plugin's name, description and list of arguments and default values, for exemple:
``` javascript
{ "test":
  { "description":
    "a standard test tone (sine)",
  "arguments":
    { "sfreq": 200 }, "srcName": "testtone"
  }
}
```

`/satie/responder ip port`

- *ip*, *port* are a string and int

<pre class=note>
<span class=warning>WARNING:</span> Keep in mind, however, that prior to any OSC communication, the default destination address is localhost:18060. If SATIE server is not being controlled, messages from Monitoring/Analysis destination
</pre>

`/satie/heartbeat flag`

Send indication that SATIE is running by emitting a periodic heartbeat OSC message to SATIE's responder address.
The heartbeat message sent is */satie.heartbeat*. This feature is disable by default.

- flag: 0 to disable, 1 to enable.

`/satie/plugindetails synthdefName  // get arguments of some synthdef`

- synthdefName string

Like above, SATIE responds with an osc message: /arguments JSON string representing plugin's name, description and list of arguments, their types and default values, wrapped in a dictionary like the example below:
``` javascript
{ "misDrone":
    { "description": "a rich drone sound", "arguments":
        [ { "name": "freq", "value": 200, "type": "Integer" },
            { "name": "dur", "value": 22, "type": "Integer" },
            { "name": "amp", "value": 0.75, "type": "Float" } ],
        "srcName": "misDrone"
    }
}
```

<pre class=note>
<span class=note text>Note:</span> by default, the response is sent to ip:port of the sender. It can be set permanently for the duration of the session via by
sending <span class=.code>/satie/responder</span> a message containing IP and port.
</pre>

## Monitoring/Analysis

The following messages are sent by select "monitoring" plugins (see _Analysers_ section in the _SATIE Overview->SATIE Plugins_ of the Quark documentation):

`/trigger/\<instance_name\> value`

   forward trigger messages from `SendTrig`

   OSC message sent to the above URI with the current amplitude envelope value (a float)

`/analysis/\<instance name\> hasFreq envelope frequency midinote_number`

   forward messages received from `SendReply`

   OSC message sent to the above URI with following parameters:

- hasFreq: whether the trigger has a measurable frequency (0, 1)
- envelope: the current value of the amplitude envelope follower (float)
- frequency: measured note frequency (Hz)
- midinote_number: frequency converted to MIDI Note value

These two are provided as convenience functionality for getting information from running synths.
The SendTrig and SendReply UGens can, of course, be used in your SynthDefs but SATIE can load plugins specially designed to measure signals and send triggers of stream's measured data.

## Renderer Messages

These messages are used to control the master output of Satie.

`/satie/renderer/setOrientationDeg <azimuth_deg> <elevation_deg>`

Sets the orientation offset of the listener, equivalent to rotating the listener.

`/satie/renderer/setOutputDB <outputVolume_dB>`

Sets the output level in dB.

`/satie/renderer/setOutputTrimDB <outputVolumeTrim_dB>`

Changes the output level by the given amount in dB.

`/satie/renderer/setOutputMute \<mute>`

- `mute` = 1: output sound is muted,
- `mute` = 0: output sound is unmuted.

`/satie/renderer/setOutputDim \<dim>`

- `dim` = 1: output level is set to - 30dB,
- `dim` = 0: output level is reset to its previous value.

`/satie/renderer/freeSynths \<groupName>`

Frees all synths in the given group.
