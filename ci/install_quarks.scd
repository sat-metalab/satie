var args = thisProcess.argv;
var path;

if(args.size !== 1) {
    1.exit;
    "\n\nERROR: Incorrect number of arguments. Expected a path to a Quark folder.\n\nexample:\n\t$ sclang install_quarks.scd /home/user/myQuark\n\n".postln
} {
    path = args[0];
};

fork {
    if (File.exists(path +/+ "SATIE.quark")) {
        Quarks.install(path);
    } {
        "SATIE quark file not found at: %".format(path).postln
    };
    5.wait;
    0.exit
}
